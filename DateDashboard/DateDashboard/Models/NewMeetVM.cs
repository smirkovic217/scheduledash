﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DateDashboard.Models
{
    public class NewMeetVM
    {
        public string Person { get; set; }
        public DateTime From { get; set; }
    }
}
