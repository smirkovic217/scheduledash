﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DateDashboard.Models
{
    public class EditMeetVM
    {
        public string Person { get; set; }
        public Meeting Meeting { get; set; }        
    }
}
