﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DateDashboard.Models
{
    public class Person
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
