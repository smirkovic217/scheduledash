﻿using System;
using Telerik.XamarinForms.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DateDashboard.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DayPage : ContentPage
	{
		public DayPage ()
		{
			InitializeComponent ();
		}

        private void CalendarLoaded(object sender, EventArgs e)
        {
            RadCalendar calendar = sender as RadCalendar;
            calendar.TrySetViewMode(CalendarViewMode.Day, false);

            calendar.CellTapped += Calendar_CellTapped;
            calendar.AppointmentTapped += Calendar_AppointmentTapped;
            
        }

        private void Calendar_AppointmentTapped(object sender, AppointmentTappedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Calendar_CellTapped(object sender, CalendarCell e)
        {
            throw new NotImplementedException();
        }

        //private void CalendarLoaded(object sender, EventArgs e)
        //{
        //    var calendar = sender as RadCalendar;
        //   // calendar.TrySetViewMode(CalendarViewMode.Day, true);
        //}
    }
}