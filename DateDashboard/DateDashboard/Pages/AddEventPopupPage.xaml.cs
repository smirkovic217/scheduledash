﻿using Rg.Plugins.Popup.Pages;
using Xamarin.Forms.Xaml;

namespace DateDashboard.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddEventPopupPage : PopupPage
    {
		public AddEventPopupPage ()
		{
			InitializeComponent ();
		}
	}
}