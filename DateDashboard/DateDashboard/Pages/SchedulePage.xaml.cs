﻿using Syncfusion.SfSchedule.XForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DateDashboard.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SchedulePage : ContentPage
	{
		public SchedulePage ()
		{
			InitializeComponent ();

            schedule.ScheduleView = ScheduleView.DayView;
            DayViewSettings dayViewSettings = new DayViewSettings();
            DayLabelSettings dayLabelSettings = new DayLabelSettings();
            dayLabelSettings.TimeFormat = "hh mm a";
            dayViewSettings.DayLabelSettings = dayLabelSettings;
            dayViewSettings.StartHour = DateTime.Now.Hour;
            schedule.DayViewSettings = dayViewSettings;
        }
    }
}