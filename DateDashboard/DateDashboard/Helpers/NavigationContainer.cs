﻿using FreshMvvm;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DateDashboard.Helpers
{
    public class NavigationContainer : Xamarin.Forms.NavigationPage, IFreshNavigationService
    {
        public NavigationContainer(Page page)
    : this(page, Constants.DefaultNavigationServiceName)
        {
        }

        public NavigationContainer(Page page, string navigationPageName)
            : base(page)
        {
            var pageModel = page.GetModel();
            pageModel.CurrentNavigationServiceName = navigationPageName;
            NavigationServiceName = navigationPageName;
            RegisterNavigation();
        }

        protected void RegisterNavigation()
        {
            FreshIOC.Container.Register<IFreshNavigationService>(this, NavigationServiceName);
        }

        internal Page CreateContainerPageSafe(Page page)
        {
            if (page is NavigationPage || page is MasterDetailPage || page is TabbedPage)
                return page;

            return CreateContainerPage(page);
        }

        protected virtual Page CreateContainerPage(Page page)
        {
            return new NavigationPage(page);
        }

        public Task PushPage(Page page, FreshBasePageModel model, bool modal = false, bool animate = true)
        {
            if (page is PopupPage popupPage)
                return Navigation.PushPopupAsync(popupPage);
            if (modal)
                return this.Navigation.PushModalAsync(CreateContainerPageSafe(page));
            return this.Navigation.PushAsync(page);
        }

        public Task PopPage(bool modal = false, bool animate = true)
        {
            if (PopupNavigation.Instance?.PopupStack?.Count > 0)
                return Navigation.PopPopupAsync();
            if (modal)
                return this.Navigation.PopModalAsync(animate);
            return this.Navigation.PopAsync(animate);
        }

        public Task PopToRoot(bool animate = true)
        {
            return Navigation.PopToRootAsync(animate);
        }

        public string NavigationServiceName { get; private set; }

        public void NotifyChildrenPageWasPopped()
        {
            this.NotifyAllChildrenPopped();
        }

        public Task<FreshBasePageModel> SwitchSelectedRootPageModel<T>() where T : FreshBasePageModel
        {
            throw new Exception("This navigation container has no selected roots, just a single root");
        }
    }
}
