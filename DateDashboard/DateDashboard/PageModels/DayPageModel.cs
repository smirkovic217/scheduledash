﻿using AutoMapper;
using DateDashboard.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace DateDashboard.PageModels
{
    public class DayPageModel : BasePageModel
    {
        private Person _selectedPerson;

        // public ObservableCollection<EventData> Events { get; set; }        
        public ObservableCollection<Person> PersonsSource { get; set; }
        public ObservableCollection<EventData> Appointments { get; set; }
        public DateTime SelectedDate { get; set; }

        public Person SelectedPerson
        {
            get => _selectedPerson;
            set
            {
                //Change source when selected person changed
                _selectedPerson = value;
                Appointments = GetAppointmentSourceForPerson(SelectedPerson.Name);
                
            }
        }

        public DayPageModel()
        {
            //Appointments = CreateAppointments();
        }

        public override void Init(object initData)
        {
            PersonsSource = GetPersonsSource();
            SelectedPerson = PersonsSource[0];

            if (initData is ObservableCollection<EventData> events)
            {
                Appointments = events;// Mapper.Map< ObservableCollection<Appointment>>(events);
                SelectedDate = events[0].StartDate;
            }

            base.Init(initData);
        }

        public Command AddAppointmentCommand => new Command(async () =>
        {
            await CoreMethods.PushPageModel<AddEventPopupPageModel>();
        });

        #region Data helpers
        private ObservableCollection<Person> GetPersonsSource()
        {
            return new ObservableCollection<Person>
            {
                new Person {Name = "Mika Mikic"},
                new Person {Name = "Pera Mikic"},
                new Person {Name = "Mika Peric"},
                new Person {Name = "Pera Peric"},
                new Person {Name = "Zika Zikic"},
            };
        }

        private ObservableCollection<EventData> GetAppointmentSourceForPerson(string name)
        {
            switch (name)
            {
                case "Mika Mikic":
                case "Mika Peric":
                case "Zika Zikic":
                    return new ObservableCollection<EventData>
                        {
                            new EventData(
                                SelectedDate,
                                SelectedDate.AddHours(2),
                                "Tokyo Deall call",
                                Color.FromHex("FFA200"),
                                Color.White),
                            new EventData(
                                SelectedDate.AddHours(1) ,
                                SelectedDate.AddHours(3).AddMinutes(30),
                                "Dinner with the Morgans",
                                Color.FromHex("C42FBA"),
                                Color.White),
                        };
                case "Pera Mikic":
                case "Pera Peric":                
                    return new ObservableCollection<EventData>
                        {
                            new EventData(
                                SelectedDate,
                                SelectedDate.AddHours(4),
                                "Conference call with HQ2",
                                Color.FromHex("FFA200"),
                                Color.White),
                            new EventData(
                                SelectedDate.AddHours(1) ,
                                SelectedDate.AddHours(5).AddMinutes(45),
                                "Weekend barbecue",
                                Color.FromHex("59B6B8"),
                                Color.FromHex("59B6B8")),
                            new EventData(
                                SelectedDate.AddHours(6) ,
                                SelectedDate.AddHours(7),
                                "Weekend barbecue",
                                Color.FromHex("C9353E"),
                                Color.FromHex("C9353E"))
                        };
                default:
                    return new ObservableCollection<EventData>();
            }
        }

        private ObservableCollection<Appointment> CreateAppointments()
        {
            var work = Color.FromHex("C9353E");
            var home = Color.FromHex("59B6B8");
            var other = Color.FromHex("C42FBA");
            var party = Color.FromHex("FFA200");

            return new ObservableCollection<Appointment>
            {
                // today

                new Appointment(
                    DateTime.Today.AddHours(8).AddMinutes(30),
                    DateTime.Today.AddHours(9).AddMinutes(30),
                    "Fitness with Peter",
                    "@TopFit",
                    home),
                new Appointment(
                    DateTime.Today.AddHours(11),
                    DateTime.Today.AddHours(11).AddMinutes(15),
                    "Daily SCRUM", "",
                    work),
                new Appointment(
                    DateTime.Today.AddHours(11),
                    DateTime.Today.AddHours(12),
                    "Job Interview", "HQ1 213",
                    work),
                new Appointment(
                    DateTime.Today.AddHours(12).AddMinutes(30),
                    DateTime.Today.AddHours(14),
                    "Lunch with Sam", "Skara Bira",
                    party),
                new Appointment(
                    DateTime.Today.AddHours(14).AddMinutes(30),
                    DateTime.Today.AddHours(15).AddMinutes(30),
                    "Webinar", "@work",
                    work),
                new Appointment(
                    DateTime.Today.AddHours(16),
                    DateTime.Today.AddHours(17).AddMinutes(30),
                    "Tokio Deal call", "",
                    work),
                new Appointment(
                    DateTime.Today.AddHours(16).AddMinutes(45),
                    DateTime.Today.AddHours(17).AddMinutes(45),
                    "Dentist", "Dental Clinic",
                    home),
                new Appointment(
                    DateTime.Today.AddHours(18),
                    DateTime.Today.AddHours(19).AddMinutes(30),
                    "Home theater evening", "Samanta",
                    other),
                new Appointment(
                    DateTime.Today.AddHours(20),
                    DateTime.Today.AddHours(22),
                    "Watch a movie", "Cinema City",
                    party),
                new Appointment(
                    DateTime.Today ,
                    DateTime.Today.AddMinutes(1),
                    "Alex's Birthday", "Cinema City",
                    party, true),
                // -1
                new Appointment(
                    DateTime.Today.AddDays(-1).AddHours(10),
                    DateTime.Today.AddDays(-1).AddHours(11).AddMinutes(30),
                    "Innovation Explorer " + DateTime.Today.AddDays(-1).Year, "Sofia Event Center",
                    home),
                new Appointment(
                    DateTime.Today.AddDays(-1).AddHours(18),
                    DateTime.Today.AddDays(-1).AddHours(19),
                    "Swimming", "Holiday Inn",
                    work),
                new Appointment(
                    DateTime.Today.AddDays(-1).AddHours(13),
                    DateTime.Today.AddDays(-1).AddHours(14).AddMinutes(30),
                    "Lunch with the Morgans", "",
                    home),
                new Appointment(
                    DateTime.Today.AddDays(-1).AddHours(20),
                    DateTime.Today.AddDays(-1).AddHours(22).AddMinutes(30),
                    "Theater evening", "City Theater",
                    party),
                new Appointment(
                    DateTime.Today.AddDays(-1).AddHours(15),
                    DateTime.Today.AddDays(-1).AddHours(16),
                    "Conference call with HQ2", "",
                    work),
                new Appointment(
                    DateTime.Today.AddDays(2),
                    DateTime.Today.AddDays(3),
                    "Movies Festival", "",
                    other, true),
                // -2
                new Appointment(
                    DateTime.Today.AddDays(-2),
                    DateTime.Today.AddDays(-2).AddSeconds(1),
                    "Team Building Barbecue", "Tokio",
                    party, true),
                new Appointment(
                    DateTime.Today.AddDays(-2),
                    DateTime.Today.AddDays(-2).AddSeconds(1),
                    "Mountain biking",
                    "Vitosha",
                    other, true),
                // +1
                new Appointment(
                    DateTime.Today.AddDays(1).AddHours(20),
                    DateTime.Today.AddDays(1).AddHours(22),
                    "Date with Candice", "Happy Bar&Grill",
                    home),
                // +2
                new Appointment(
                    DateTime.Today.AddDays(2).AddHours(16).AddMinutes(30),
                    DateTime.Today.AddDays(2).AddHours(17),
                    "Daily meeting", "",
                    work),
                new Appointment(
                    DateTime.Today.AddDays(2).AddHours(18),
                    DateTime.Today.AddDays(2).AddHours(19).AddMinutes(30),
                    "Favourite show", "@Home",
                    home),
                new Appointment(
                    DateTime.Today.AddDays(2).AddHours(19),
                    DateTime.Today.AddDays(2).AddHours(20).AddMinutes(30),
                    "Football", "National Stadium",
                    party),
                new Appointment(
                    DateTime.Today.AddDays(2).AddHours(19),
                    DateTime.Today.AddDays(2).AddHours(20).AddMinutes(30),
                    "Planting Trees", "Rila Mountain",
                    other, true),
                new Appointment(
                    DateTime.Today.AddDays(2),
                    DateTime.Today.AddDays(10),
                    "Vacation", "Florence",
                    party, true),
                // +3
                new Appointment(
                    DateTime.Today.AddDays(3).AddHours(10),
                    DateTime.Today.AddDays(3).AddHours(11),
                    "Coordination meeting", "HQ3 205",
                    work),
                new Appointment(
                    DateTime.Today.AddDays(3).AddHours(15),
                    DateTime.Today.AddDays(3).AddHours(16).AddMinutes(30),
                    "Table tennis", "",
                    party),
                new Appointment(
                     DateTime.Today.AddDays(3).AddHours(18),
                     DateTime.Today.AddDays(3).AddHours(19).AddMinutes(30),
                     "Theater evening", "",
                     party),
                // +4
                new Appointment(
                    DateTime.Today.AddDays(4).AddHours(9),
                    DateTime.Today.AddDays(4).AddHours(10),
                    "Conference call with Tokio", "HQ1 Sofia",
                    work),
                // +5
                new Appointment(
                    DateTime.Today.AddDays(5).AddHours(21),
                    DateTime.Today.AddDays(5).AddHours(23),
                    "Birthday party",
                    "@Sam",
                    party)
            };
        }
        #endregion
    }
}
