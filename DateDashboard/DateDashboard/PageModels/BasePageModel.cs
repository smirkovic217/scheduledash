﻿using FreshMvvm;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DateDashboard.PageModels
{
    public class BasePageModel : FreshBasePageModel
    {
        public override void Init(object initData)
        {
            base.Init(initData);
            var task = Task.Run(() => InitializeAsync(initData));
        }

        public virtual void InitializeAsync(object initData) { }
    }
}
