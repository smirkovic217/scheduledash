﻿using DateDashboard.Models;
using Syncfusion.SfSchedule.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace DateDashboard.PageModels
{
    public class SchedulePageModel : BasePageModel
    {
        private Person _selectedPerson;

        public ObservableCollection<Meeting> Meetings { get; set; }
        public ObservableCollection<Person> PersonsSource { get; set; }
        public Person SelectedPerson
        {
            get => _selectedPerson;
            set
            {
                _selectedPerson = value;
                Meetings = GetMeetingsForPerson(_selectedPerson.Name);
            }
        }
        public ICommand ScheduleCellDoubleTapped { get; set; }

        public SchedulePageModel()
        {
            ScheduleCellDoubleTapped = new Command<CellTappedEventArgs>(DoubleTapped);
        }

        public override void Init(object initData)
        {
            Meetings = new ObservableCollection<Meeting>();
            base.Init(initData);
        }

        public override void InitializeAsync(object initData)
        {
            base.InitializeAsync(initData);

            PersonsSource = GetPersonsSource();
            if (PersonsSource != null && PersonsSource.Count > 0)
            {
                SelectedPerson = PersonsSource[0];
                Meetings = GetMeetingsForPerson(SelectedPerson.Name);
            }
        }

        public override void ReverseInit(object returnedData)
        {
            base.ReverseInit(returnedData);

            if (returnedData is Meeting meeting)
            {
                Meetings.Add(meeting);
            }
            //else if (returnedData is EditMeetVM editVM)
            //{
            //    Meetings.Clear();
            //    Meetings = new ObservableCollection<Meeting>();
            //    foreach (var item in Meetings)
            //    {
            //        if (item.Id == editVM.Meeting.Id)
            //        {
            //            item.EventName = editVM.Meeting.EventName;
            //        }
                    
            //        Meetings.Add(item);
            //    }
            //}
        }

        private void DoubleTapped(CellTappedEventArgs args)
        {
            if (args.Appointment != null)
            {
                var appoint = args.Appointment as Meeting;
                var edit = new EditMeetVM
                {
                    Meeting = appoint,
                    Person = SelectedPerson.Name
                };
                CoreMethods.PushPageModel<AddEventPopupPageModel>(edit);
            }
            else
            {
                var selectedDateTime = args.Datetime;

                var newMeetInfo = new NewMeetVM
                {
                    Person = SelectedPerson.Name,
                    From = args.Datetime
                };
                CoreMethods.PushPageModel<AddEventPopupPageModel>(newMeetInfo);
            }                        
        }

        private ObservableCollection<Person> GetPersonsSource()
        {
            return new ObservableCollection<Person>
            {
                new Person {Name = "Mika Mikic"},
                new Person {Name = "Pera Mikic"},
                new Person {Name = "Mika Peric"},
                new Person {Name = "Pera Peric"},
                new Person {Name = "Zika Zikic"},
                new Person {Name = "Mika Mikic"},
                new Person {Name = "Pera Mikic"},
                new Person {Name = "Mika Peric"},
                new Person {Name = "Pera Peric"},
                new Person {Name = "Zika Zikic"},
                new Person {Name = "Mika Mikic"},
                new Person {Name = "Pera Mikic"},
                new Person {Name = "Mika Peric"},
                new Person {Name = "Pera Peric"},
                new Person {Name = "Zika Zikic"},
            };
        }

        private ObservableCollection<Meeting> GetMeetingsForPerson(string person)
        {
            var meetings = new ObservableCollection<Meeting>();

            switch (person)
            {
                case "Mika Mikic":
                case "Mika Peric":
                case "Zika Zikic":
                    return new ObservableCollection<Meeting> {
                        new Meeting {
                            EventName = "General Meeting", AllDay = true, Color = Color.FromHex("#FF339933"),
                            From = DateTime.Today.AddHours(8), To = DateTime.Today.AddHours(9), Id = 1
                        },
                        new Meeting {
                            EventName = "Plan Execution", AllDay = false, Color = Color.FromHex("#FF00ABA9"),
                            From = DateTime.Today.AddHours(10), To = DateTime.Today.AddHours(11).AddMinutes(20), Id = 2
                        },
                        new Meeting {
                            EventName = "Project Plan", AllDay = false, Color = Color.FromHex("#FFE671B8"),
                            From = DateTime.Today.AddHours(12), To = DateTime.Today.AddHours(13), Id = 3
                        },
                        new Meeting {
                            EventName = "Consulting", AllDay = false, Color = Color.FromHex("#FF1BA1E2"),
                            From = DateTime.Today.AddHours(16), To = DateTime.Today.AddHours(18).AddMinutes(45), Id = 4
                        },
                        new Meeting {
                            EventName = "Performance Check", AllDay = false, Color = Color.FromHex("#FFD80073"),
                            From = DateTime.Today.AddDays(1).AddHours(10), To = DateTime.Today.AddDays(1).AddHours(11), Id = 5
                        },
                        new Meeting {
                            EventName = "Yoga Therapy", AllDay = false, Color = Color.FromHex("#FFA2C139"),
                            From = DateTime.Today.AddDays(1).AddHours(13), To = DateTime.Today.AddDays(1).AddHours(14).AddMinutes(30), Id = 6
                        },
                        new Meeting {
                            EventName = "Plan Execution", AllDay = false, Color = Color.FromHex("#FF339933"),
                            From = DateTime.Today.AddDays(2).AddHours(10), To = DateTime.Today.AddDays(2).AddHours(11), Id = 7
                        },
                        new Meeting {
                            EventName = "Project Plan", AllDay = false, Color = Color.FromHex("#FFD80073"),
                            From = DateTime.Today.AddDays(-1).AddHours(10), To = DateTime.Today.AddDays(-1).AddHours(11), Id = 8
                        },
                        new Meeting {
                            EventName = "General Meeting", AllDay = false, Color = Color.FromHex("#FFE671B8"),
                            From = DateTime.Today.AddDays(-1).AddHours(15), To = DateTime.Today.AddDays(-1).AddHours(17), Id = 9
                        },
                    };
                default:
                    return new ObservableCollection<Meeting> {
                        new Meeting {
                            EventName = "Tokyo Deall call", AllDay = true, Color = Color.FromHex("#FF339933"),
                            From = DateTime.Today.AddHours(8), To = DateTime.Today.AddHours(9), Id = 10
                        },
                        new Meeting {
                            EventName = "Dinner with the Morgans", AllDay = false, Color = Color.FromHex("#FF00ABA9"),
                            From = DateTime.Today.AddHours(10), To = DateTime.Today.AddHours(11).AddMinutes(20), Id = 11
                        },
                        new Meeting {
                            EventName = "Theater evening", AllDay = false, Color = Color.FromHex("#FFE671B8"),
                            From = DateTime.Today.AddHours(12), To = DateTime.Today.AddHours(13), Id = 12
                        },
                        new Meeting {
                            EventName = "Consulting", AllDay = true, Color = Color.FromHex("#FF1BA1E2"),
                            From = DateTime.Today.AddHours(16), To = DateTime.Today.AddHours(18).AddMinutes(45), Id = 13
                        },
                        new Meeting {
                            EventName = "Conference call with HQ2", AllDay = false, Color = Color.FromHex("#FFD80073"),
                            From = DateTime.Today.AddDays(1).AddHours(10), To = DateTime.Today.AddDays(1).AddHours(11), Id = 14
                        },
                        new Meeting {
                            EventName = "Weekend barbecue", AllDay = false, Color = Color.FromHex("#FFA2C139"),
                            From = DateTime.Today.AddDays(1).AddHours(13), To = DateTime.Today.AddDays(1).AddHours(14).AddMinutes(30), Id = 15
                        },
                        new Meeting {
                            EventName = "Mountain biking", AllDay = false, Color = Color.FromHex("#FF339933"),
                            From = DateTime.Today.AddDays(2).AddHours(10), To = DateTime.Today.AddDays(2).AddHours(11), Id = 16
                        },
                        new Meeting {
                            EventName = "Yachting", AllDay = false, Color = Color.FromHex("#FFD80073"),
                            From = DateTime.Today.AddDays(-1).AddHours(10), To = DateTime.Today.AddDays(-1).AddHours(11), Id = 17
                        },
                        new Meeting {
                            EventName = "Date with Candice", AllDay = true, Color = Color.FromHex("#FFE671B8"),
                            From = DateTime.Today.AddDays(-1).AddHours(15), To = DateTime.Today.AddDays(-1).AddHours(17), Id = 18
                        },
                    };
            }
        }
    }
}
