﻿using DateDashboard.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace DateDashboard.PageModels
{
    public class AddEventPopupPageModel : BasePageModel
    {
        private bool _isEdit;
        public string PersonName { get; set; }
        public string Title { get; set; }
        public DateTime MeetDateFrom { get; set; }
        public DateTime MeetDateTo { get; set; }
        public TimeSpan TimeFrom { get; set; }
        public TimeSpan TimeTo { get; set; }
        private EditMeetVM _event;

        public ObservableCollection<Person> PersonsSource { get; set; }
        public Person SelectedPerson { get; set; }

        public AddEventPopupPageModel()
        {
            _event = new EditMeetVM();
        }

        public override void Init(object initData)
        {
            PersonsSource = GetPersonsSource();
            SelectedPerson = PersonsSource[0];

            if (initData is NewMeetVM newMeet)
            {
                _isEdit = false;
                PersonName = newMeet.Person;
                MeetDateFrom = newMeet.From;
                TimeFrom = new TimeSpan(MeetDateFrom.Hour, MeetDateFrom.Minute, MeetDateFrom.Second);
            }
            else if (initData is EditMeetVM editMeet)
            {
                _event = editMeet;
                _isEdit = true;
                PersonName = editMeet.Person;
                Title = editMeet.Meeting.EventName;
                MeetDateFrom = editMeet.Meeting.From;
                MeetDateTo = editMeet.Meeting.To;
                TimeFrom = new TimeSpan(MeetDateFrom.Hour, MeetDateFrom.Minute, MeetDateFrom.Second);
                TimeTo = new TimeSpan(MeetDateTo.Hour, MeetDateTo.Minute, MeetDateTo.Second);
            }
            
            base.Init(initData);
        }

        public Command SaveCommand => new Command(async () =>
        {
            if (_isEdit)
            {
                var meet = new EditMeetVM
                {
                    Meeting = new Meeting {Id = _event.Meeting.Id, EventName = Title, From = MeetDateFrom, To = MeetDateTo },
                };

                await CoreMethods.PopPageModel(meet);
            }
            else
            {
                var appoint = new Meeting
                {
                    EventName = Title,
                    AllDay = false,
                    Color = Color.LightSkyBlue,
                    From = MeetDateFrom,
                    To = new DateTime(MeetDateFrom.Year, MeetDateFrom.Month, MeetDateFrom.Day, TimeTo.Hours, TimeTo.Minutes, TimeTo.Seconds)
                };
                await CoreMethods.PopPageModel(appoint);
            }                        
        });

        private ObservableCollection<Person> GetPersonsSource()
        {
            return new ObservableCollection<Person>
            {
                new Person {Name = "Mika Mikic"},
                new Person {Name = "Pera Mikic"},
                new Person {Name = "Mika Peric"},
                new Person {Name = "Pera Peric"},
                new Person {Name = "Zika Zikic"},
            };
        }
    }
}
