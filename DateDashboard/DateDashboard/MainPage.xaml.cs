﻿using DateDashboard.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.XamarinForms.Input;
using Xamarin.Forms;

namespace DateDashboard
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            calendar.AppointmentTapped += Calendar_AppointmentTapped;
        }

        private async void Calendar_AppointmentTapped(object sender, AppointmentTappedEventArgs e)
        {
            var page = new DayPage();
            await this.Navigation.PushAsync(page);
        }
    }
}
