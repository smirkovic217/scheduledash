﻿using AutoMapper;
using DateDashboard.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DateDashboard
{
    class Mappings
    {
        public static void Init()
        {
            try
            {
                Mapper.Initialize(cfg =>
                {
                    cfg.CreateMap<EventData, Appointment>()
                    .ForMember(dest => dest.StartDate, opts => opts.MapFrom(src => src.StartDate))
                    .ForMember(dest => dest.EndDate, opts => opts.MapFrom(src => src.EndDate))
                    .ForMember(dest => dest.IsAllDay, opts => opts.MapFrom(src => src.IsAllDay))
                    .ForMember(dest => dest.Title, opts => opts.MapFrom(src => src.Title))
                    .ForMember(dest => dest.Detail, opts => opts.MapFrom(src => src.Detail))
                    .ForMember(dest => dest.Color, opts => opts.MapFrom(src => src.Color));
                });
            }
            catch (Exception ex)
            {
            }
        }
    }
}
