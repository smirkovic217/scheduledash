using DateDashboard.Helpers;
using DateDashboard.PageModels;
using FreshMvvm;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace DateDashboard
{
	public partial class App : Application
	{
        public const string NavigationService = "NavigationService";
        private NavigationContainer navContainer;

        public App ()
		{
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NzMxNUAzMTM2MmUzMjJlMzBmNTdwWlp0ZVZkdjBubDU5bnN4T2ZweHFJUFN2OGlRZ08wRFgxMnZRcDNVPQ==");

            InitializeComponent();
            Mappings.Init();

            var page = FreshPageModelResolver.ResolvePageModel<SchedulePageModel>();
            navContainer = new NavigationContainer(page, NavigationService);
            MainPage = navContainer;
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
